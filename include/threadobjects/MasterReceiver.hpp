#ifndef PINFS_THREADOBJECTS_MASTER_RECEIVER_HPP
#define PINFS_THREADOBJECTS_MASTER_RECEIVER_HPP

#include <boost/asio.hpp>
#include <string>

#include "threadobjects/Manager.hpp"
#include "concurrent/SynchronizedQueue.hpp"

namespace pinfs {
namespace threadobjects {

class MasterReceiver {
public:
  MasterReceiver(Manager& manager)
    : manager_(manager) {
  }

  void addToQueue(const std::string& encodedmsg);

  void executor();

private:
  Manager& manager_;

  concurrent::SynchronizedQueue<std::string> queue_;
};

  void receiverWorker(MasterReceiver& mr, boost::asio::io_context& ioc, unsigned short port);

} //threadobjects
} //pinfs

#endif //PINFS_THREADOBJECTS_MASTER_RECEIVER_HPP
