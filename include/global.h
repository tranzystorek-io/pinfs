#ifndef PINFS_GLOBAL
#define PINFS_GLOBAL

//key length in bytes
#ifndef PINFS_KEY_LENGTH
#define PINFS_KEY_LENGTH 20
#endif //PINFS_KEY_LENGTH

//bucket size
#ifndef PINFS_BUCKET_SIZE
#define PINFS_BUCKET_SIZE 20
#endif //PINFS_BUCKET_SIZE

//number of routing table bucket slots
#define PINFS_N_BUCKETS (PINFS_KEY_LENGTH * 8)

//number of concurrent sender threads
#define PINFS_ALPHA_FACTOR 3

//number of maximum unimproved requests for a command session
#define PINFS_MAX_UNIMPROVED_REQUESTS 0

//size of null terminator
#define PINFS_NULL_TERMINATOR_SIZE 1

//field width definitions
#define PINFS_LONG_FIELD_WIDTH 8
#define PINFS_INT_FIELD_WIDTH 4
#define PINFS_SHORT_FIELD_WIDTH 2
#define PINFS_CHAR_FIELD_WIDTH 1

#define DEFAULT_PORT 50000

#endif //PINFS_GLOBAL
