#ifndef PINFS_PUTEVENT_HPP
#define PINFS_PUTEVENT_HPP

#include "events/Event.hpp"

namespace pinfs {
namespace events {

struct PutEvent : public Event {
  std::string fileName;

  PutEvent(const std::string& fname)
    : Event(Event::PUT),
      fileName(fname) {
  }
};

} //events
} //pinfs

#endif //PINFS_PUTEVENT_HPP
