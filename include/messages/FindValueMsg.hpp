#ifndef PINFS_FIND_VALUE_MSG_HPP
#define PINFS_FIND_VALUE_MSG_HPP

#include "Contact.hpp"
#include "messages/Message.hpp"

#include <vector>

namespace pinfs {
namespace messages {

struct FindValueRequest : public Message {
  Key searched;

  FindValueRequest(int id, const Contact& contact)
    : Message(Message::FVALUE_REQ, id, contact) {
  }

  std::string serialize() override;
};

struct FindValueResponse : public Message {
  enum ResponseType : unsigned char {
    NOFILE,
    FILE
  } rtype;

  std::vector<Contact> nearest;
  std::vector<unsigned char> filecontent;

  FindValueResponse(int id, const Contact& contact, ResponseType type = ResponseType::NOFILE)
    : Message(Message::FVALUE_RES, id, contact),
      rtype(type) {
  }

  std::string serialize() override;
};

} //messages
} //pinfs

#endif //PINFS_FIND_VALUE_MSG_HPP
