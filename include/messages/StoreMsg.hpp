#ifndef PINFS_STORE_MSG_HPP
#define PINFS_STORE_MSG_HPP

#include "messages/Message.hpp"
#include "events/Event.hpp"

#include <vector>

namespace pinfs {
namespace messages {

struct StoreRequest : public Message {
  std::string filename;
  std::vector<unsigned char> filecontent;

  StoreRequest(int id, const Contact& contact, const std::string& fname = "")
    : Message(Message::STORE_REQ, id, contact),
      filename(fname) {
  }

  std::string serialize() override;
};

  struct StoreResponse : public Message {
  StoreResponse(int id, const Contact& contact)
    : Message(Message::STORE_RES, id, contact) {
  }

  std::string serialize() override;
};

} //messages
} //pinfs

#endif //PINFS_STORE_MSG_HPP
