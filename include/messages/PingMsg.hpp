#ifndef PINFS_PING_MSG_HPP
#define PINFS_PING_MSG_HPP

#include "messages/Message.hpp"

namespace pinfs {
namespace messages {

struct PingRequest : public Message {
  PingRequest(int id, const Contact& contact)
    : Message(Message::PING_REQ, id, contact) {
  }

  std::string serialize() override;
};

  struct PingResponse : public Message {
  PingResponse(int id, const Contact& contact)
    : Message(Message::PING_RES, id, contact) {
  }

  std::string serialize() override;
};

} //messages
} //pinfs

#endif //PINFS_PING_MSG_HPP
