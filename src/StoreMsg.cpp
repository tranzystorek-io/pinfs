#include "messages/StoreMsg.hpp"

#include "utility.hpp"

using namespace pinfs::messages;

std::string StoreRequest::serialize() {
  std::string ret;
  int size = msgHeaderSize()
    + PINFS_INT_FIELD_WIDTH + filename.size()
    + filecontent.size();

  ret += makeHeader(size);
  ret.append(reinterpret_cast<const char*>(utility::ToBytes<int, PINFS_INT_FIELD_WIDTH>(filename.size()).data()), PINFS_INT_FIELD_WIDTH);
  ret += filename;
  ret.append(reinterpret_cast<const char*>(filecontent.data()), filecontent.size());

  return ret;
}

std::string StoreResponse::serialize() {
  return makeHeader(msgHeaderSize());
}
