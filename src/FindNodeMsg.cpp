#include "messages/FindNodeMsg.hpp"

#include "global.h"
#include "utility.hpp"

using namespace pinfs::messages;

std::string FindNodeRequest::serialize() {
  std::string ret;
  const int size = msgHeaderSize()
    + sizeof(pinfs::Key);

  ret += makeHeader(size);
  ret.append(reinterpret_cast<const char*>(searched.bytes.data()), PINFS_KEY_LENGTH);

  return ret;
}

std::string FindNodeResponse::serialize() {
  std::string ret;
  const int size = msgHeaderSize()
    + nearest.size() * (PINFS_LONG_FIELD_WIDTH + PINFS_SHORT_FIELD_WIDTH + sizeof(pinfs::Key));

  ret += makeHeader(size);

  for(auto& contact : nearest) {
    ret.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned long, PINFS_LONG_FIELD_WIDTH>(contact.address.ip_address).data()),
               PINFS_LONG_FIELD_WIDTH);
    ret.append(reinterpret_cast<const char*>(utility::ToBytes<unsigned short, PINFS_SHORT_FIELD_WIDTH>(contact.address.port).data()),
               PINFS_SHORT_FIELD_WIDTH);
    ret.append(reinterpret_cast<const char*>(contact.id.bytes.data()),
               PINFS_KEY_LENGTH);
  }

  return ret;
}
