#include "Contact.hpp"

#include "utility.hpp"

using namespace pinfs;

std::string Contact::toHexString() {
  std::string ret;

  utility::Bytes<unsigned long> ulbytes = utility::ToBytes<unsigned long>(address.ip_address);
  ret += utility::bytesToHexString(ulbytes.data(), ulbytes.size());

  utility::Bytes<unsigned short> usbytes = utility::ToBytes<unsigned short>(address.ip_address);
  ret += utility::bytesToHexString(usbytes.data(), usbytes.size());

  ret += id.toHexString();

  return ret;
}
