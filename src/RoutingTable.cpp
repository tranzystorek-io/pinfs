#include "RoutingTable.hpp"

#include <algorithm>

using namespace pinfs;

void RoutingTable::update(const Contact& contact) {
  if(contact.id != me_.id) {
    std::cout << "Updating routing table with key: " << contact.id << std::endl;

    int prefix_length = (me_.id ^ contact.id).getPrefixLength();
    Bucket& bucket = buckets_[prefix_length];

    auto it = std::find_if(bucket.begin(), bucket.end(),
                           [this, &contact](const Contact& c) {
                             return contact.id == c.id;
                           });

    if(it == bucket.end()) {
      //add contact at the front of the bucket
      bucket.push_front(contact);
      if(bucket.size() > PINFS_BUCKET_SIZE) {
        //remove last
        bucket.pop_back();
      }
    }
    else {
      //move contact to the front of the bucket
      bucket.splice(bucket.begin(), bucket, it);
    }
  }
}

void RoutingTable::removeKey(const Key& key) {
  int prefix_length = (me_.id ^ key).getPrefixLength();
  Bucket& bucket = buckets_[prefix_length];

  bucket.remove_if([&key](const Contact& contact) {
                     return key == contact.id;
                   });
}

std::vector<Contact> RoutingTable::findKClosest(const Key& key) const {
  std::vector<Contact> ret;

  const int pos = (key ^ me_.id).getPrefixLength();
  const Bucket& bucket = buckets_[pos];

  ret.insert(ret.end(), bucket.begin(), bucket.end());
  int n = bucket.size();

  bool left, right;
  if(n < PINFS_BUCKET_SIZE)
    for(int i=1; (left = pos-i >= 0) || (right = pos+i < PINFS_N_BUCKETS); ++i) {
      bool shouldBreak = false;

      if(left) {
        const Bucket& bucket = buckets_[pos-i];

        for(auto& contact : bucket) {
          ret.push_back(contact);

          if(++n == PINFS_BUCKET_SIZE) {
            shouldBreak = true;
            break;
          }
        }
      }

      if(shouldBreak)
        break;

      if(right) {
        const Bucket& bucket = buckets_[pos+i];

        for(auto& contact : bucket) {
          ret.push_back(contact);

          if(++n == PINFS_BUCKET_SIZE) {
            shouldBreak = true;
            break;
          }
        }
      }

      if(shouldBreak)
        break;
    }

  std::cout << "FIND_K_CLOSEST returned:\n";

  for(auto& c : ret) {
    std::cout << std::hex << c.address.ip_address << std::dec
              << " " << c.address.port << " " << c.id << "\n";
  }

  std::cout << "ROUTING TABLE DIAGNOSTIC END" << std::endl;

  return ret;
}

const RoutingTable::Bucket& RoutingTable::getBucket(const pinfs::Key& key) const {
  int prefix_length = (me_.id ^ key).getPrefixLength();

  return buckets_[prefix_length];
}

const RoutingTable::BucketArray& RoutingTable::getAllBuckets() const {
  return buckets_;
}

const Key& pinfs::RoutingTable::getKey() const {
  return me_.id;
}

const Contact& RoutingTable::getContactToMe() const {
  return me_;
}

