#include "threadobjects/ControlSynchronizer.hpp"

using namespace pinfs::threadobjects;

ControlSynchronizer::Future ControlSynchronizer::getFuture() {
  promise_ = std::promise<std::string>();
  return promise_.get_future();
}

void ControlSynchronizer::synchronize(const std::string& str) {
  promise_.set_value(str);
}
